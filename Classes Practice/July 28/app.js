var express = require('express');
var home= require('./home');
var app = express();

app.use('/home', home);

app.get();

app.get('/', function(req, res){
  res.send();
});

app.listen(3000, function(){
  console.log('express http server started at 3000');
});
